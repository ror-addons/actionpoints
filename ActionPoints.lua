ActionPoints = {}

function ActionPoints.Initialize()

   CreateWindow("ActionPointsWindow", true)

   LayoutEditor.RegisterWindow( "ActionPointsWindow",
                                L"Action Points Indicator",
                                L"Displays your current AP.",
                                false, false,
                                true, nil )  

    RegisterEventHandler(SystemData.Events.PLAYER_CUR_ACTION_POINTS_UPDATED, "ActionPoints.UpdateCurrentActionPoints")
end

function ActionPoints.UpdateCurrentActionPoints()
   LabelSetText("ActionPointsWindowText", L""..GameData.Player.actionPoints.current)
end